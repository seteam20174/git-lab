Program project1;
Uses crt;
Type
  matrix=array[1..5,1..5] of integer;

Procedure CreateMatrix(var DefaultMatrix:matrix);
var
  i,j:byte;
Begin
for i:=1 to 5 do
 for j:=1 to 5 do
 DefaultMatrix[i,j]:=random(4)+1;
end;

Procedure PrintMatrix(DefaultMatrix:matrix);
var
  i,j:byte;
Begin
Clrscr;
for i:=1 to 5 do
begin
 for j:=1 to 5 do
 write(DefaultMatrix[i,j]:5);
writeln;
writeln;
end;
readln;
end;

Procedure SearchMultiply(DefaultMatrix:matrix; var DefaultMultiply:integer);
var
  i,j:byte;
Begin
DefaultMultiply:=1;
for i:=1 to 5 do
 for j:=1 to 5 do
 if i>j then
 begin
 DefaultMultiply:=DefaultMultiply*DefaultMatrix[i,j];
 end;
writeln(DefaultMultiply);
readln;
end;

Procedure SearchMinimum(DefaultMatrix:matrix;var DefaultMin:integer);
var
  i,j:byte;
Begin
DefaultMin:=DefaultMatrix[1,1];
for i:=1 to 5 do
 for j:=1 to 5 do
 begin
 if i+j<=6 then
  if DefaultMatrix[i,j]<DefaultMin then
   DefaultMin:=DefaultMatrix[i,j];
 end;
writeln(DefaultMin);
readln;
end;

Procedure ChangeMatrix(var DefaultMatrix:matrix; DefaultMin,DefaultMultiply:integer);
var
  i,j:byte;
Begin
for i:=1 to 5 do
 for j:=1 to 5 do
 begin
 if i+j<=6 then
  if DefaultMatrix[i,j]=DefaultMin then
   DefaultMatrix[i,j]:=DefaultMultiply;
 end;
end;

Procedure SearchPositiveElements(var DefaultMatrix:matrix);
var
  i,j,count:integer;
Begin
count:=0;
for i:=1 to 5 do
    for j:=1 to 5 do
    begin
    if (i+j=6) and (DefaultMatrix[i,j]>0) then
        inc(count);
    end;
writeln(count);
readln;
end;

var
  M:matrix;
  min,mult:integer;

Begin
Randomize;
CreateMatrix(M);
PrintMatrix(M);
SearchMultiply(M,mult);
SearchMinimum(M,min);
ChangeMatrix(M,min,mult);
PrintMatrix(M);
End.